package com.sanskaar.core;

import javax.persistence.*;

/**
 * Created by Sansky on 9/20/2017.
 */

@Entity
@Table(name="user")
@NamedQueries({
        @NamedQuery(name="com.sanskaar.core.User.finAll",query="select u from User u")
})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name="username")
    private String username;
    @Column(name="password")
    private String password;

   // private List<Bookmark> bookmarks = new ArrayList<>();


    public User() {
    }


    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}