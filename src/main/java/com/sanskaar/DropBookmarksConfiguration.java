package com.sanskaar;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DropBookmarksConfiguration extends Configuration {
    // TODO: implement service configuration

    @NotNull
    @Valid
    private DataSourceFactory dataSourceFactory= new DataSourceFactory();


    @JsonProperty("Database")
    public DataSourceFactory getDataSourceFactory() {
        return dataSourceFactory;
    }
}
