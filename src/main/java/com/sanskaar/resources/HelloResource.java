package com.sanskaar.resources; /**
 * Created by Sansky on 9/18/2017.
 */

import com.sanskaar.core.User;
import io.dropwizard.auth.Auth;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public  class HelloResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getResource(){

        return "Hello World";
    }
    @GET
    @Produces(MediaType.TEXT_PLAIN)
@Path("/secured")
public String getSecuredGreetings(@Auth User user){

        return "Hello Secured World";
}


}
