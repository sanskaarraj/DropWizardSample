package com.sanskaar.resources;

import com.sanskaar.core.User;
import com.sanskaar.db.UserDAO;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Sansky on 9/25/2017.
 */

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    private UserDAO dao;

    public UserResource(UserDAO dao){
        this.dao=dao;
    }

    @GET
    @UnitOfWork
    public List<User> getUser(){
        return dao.findAll();

    }
}
