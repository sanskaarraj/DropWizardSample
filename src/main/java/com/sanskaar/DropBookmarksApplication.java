package com.sanskaar;


import com.sanskaar.core.User;
import com.sanskaar.db.UserDAO;
import com.sanskaar.resources.HelloResource;
import com.sanskaar.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;



public class DropBookmarksApplication extends Application<DropBookmarksConfiguration> {

    private final HibernateBundle<DropBookmarksConfiguration> hibernateBundle =
            new HibernateBundle<DropBookmarksConfiguration>(User.class) {

                @Override
                public DataSourceFactory getDataSourceFactory(DropBookmarksConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };


    public static void main(final String[] args) throws Exception {
        new DropBookmarksApplication().run(args);
    }

    @Override
    public String getName() {
        return "DropBookmarks";
    }

    @Override
    public void initialize(final Bootstrap<DropBookmarksConfiguration> bootstrap) {
        // TODO: application initialization
            bootstrap.addBundle(new MigrationsBundle<DropBookmarksConfiguration>() {
                @Override
                public DataSourceFactory
                getDataSourceFactory(DropBookmarksConfiguration configuration) {
                return configuration.getDataSourceFactory();
                }
            });
            bootstrap.addBundle(hibernateBundle);

    }

    @Override
    public void run(final DropBookmarksConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
        environment.jersey().register(new HelloResource());
        final UserDAO userDAO=new UserDAO(hibernateBundle.getSessionFactory());
        environment.jersey().register(new UserResource(userDAO));
    }


}
