package com.sanskaar.db;

import com.sanskaar.core.User;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Sansky on 9/25/2017.
 */
public class UserDAO extends AbstractDAO<User> {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
    public List<User> findAll(){
        return list(
                namedQuery("com.sanskaar.core.User.finAll"));
    }
}
